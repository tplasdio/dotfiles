#!/usr/bin/python
# Este fichero esta en el $PYTHONSTARTUP para que
# siempre sea ejecutado al iniciar Python
# print("Running python runcom")

# Lazy importing
import os
import sys
from importlib.util import (
    find_spec as _find_spec,
    module_from_spec as _module_from_spec,
    LazyLoader as _LazyLoader
)

# Algunas funciones para navegación rápida en el REPL
# como si fuera un shell:
def clear():
    os.system('clear||cls')
    #print('\033[2J\033[H')

def pwd():
    print(os.getcwd())

def cd(directorio):
    os.chdir(directorio)

def ls():
    os.system('lsd||ls||dir')
def la():
    os.system('lsd -Alh||ls -Alh')
def ll():
    os.system('lsd -lh||ls -lh')

def notify(title="", msg=""):
    os.system(f'notify-send "{title}" "{msg}"')

def lazy_import(name: str):
    """Lazily import a module"""
    spec = _find_spec(name)
    loader = _LazyLoader(spec.loader)
    spec.loader = loader
    module = _module_from_spec(spec)
    sys.modules[name] = module
    loader.exec_module(module)
    return module

# Matplotlib
# Si encuentra la hoja de estilo 'myawesome.mplstyle', usarla
try:
	pltstyle = os.path.join(os.environ.get('XDG_CONFIG_HOME'),'matplotlib','myawesome.mplstyle')
except OSError:
	try:
		pltstyle = os.path.join(os.environ.get('HOME'),'.config','matplotlib','myawesome.mplstyle')
	except OSError:
		pass
# Úsala así: plt.style.use(pltstyle)

# from timeit import timeit
# def timee(code: str):
	# """Time how much it takes to run Python code"""
	# return timeit(stmt=code, number=1)

time = lazy_import("time")
def timestr(code: str):
	"""Time how much it takes to run Python code string"""
	start = time.perf_counter()
	exec(code)
	print("{:.21f}".format(float(time.perf_counter() - start)) )

def timefun(f, *args, **kwargs):
	"""Time how much it takes to run Python function"""
	start = time.perf_counter()
	f(*args, **kwargs)
	print("{:.21f}".format(float(time.perf_counter() - start)) )

# Lazily imports
pd = lazy_import("pandas")
np = lazy_import("numpy")  # Al usar ipython --pylab, se reimporta y levanta advertencia
plt = lazy_import("matplotlib.pyplot")
rich = lazy_import("rich")
rich = lazy_import("rich")
pag = lazy_import("pyautogui")

# Pretty colorful printing
lazy_import("rich.pretty").install()

# Si ptpython está disponible, usar dicho REPL en vez
# del REPL por omisión de Python:
#try:
#  from ptpython.repl import embed
#except ImportError:
#  print("ptpython is not available: falling back to standard prompt")
#else:
#  sys.exit(embed(globals(), locals()))
