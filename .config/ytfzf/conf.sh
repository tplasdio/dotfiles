# Usuario-Agente
if [ -f "$XDG_DATA_HOME/user_agents" ]; then
	useragent=$(grep -vi 'linux' "$XDG_DATA_HOME/user_agents" | shuf -n1)
else
	useragent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36'
fi

# Sitios que escrapear
# youtube
# peertube
# odysee
#scrape=youtube

# Opciones de yt-dlp/youtube-dl
ytdl_opts=''

#ytdl_pref="248+bestaudio/best"

# Instancias de Invidious
# https://api.invidious.io/
# vid.puffyan.us
# yewtu.be
# invidious.snopyta.org

#invidious_instance='https://vid.puffyan.us'

# Buscar en Odysee
nsfw=true

# Región
search_region=US

# Historial
#enable_hist=0
#enable_search_hist=0

# Miniaturas
async_thumbnails=1
#show_thumbnails=1
#thumbnail_quality=middle

# Desacoplar reproductor
#is_detach=1
#detach_shortcut=alt-e

# Deshabilitar atajos de fzf
print_link_shortcut=

# Otro selector
external_menu() {
	rofi -dmenu -width 1500 -p "$1"
}

