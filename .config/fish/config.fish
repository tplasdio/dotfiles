#                             ████ ██              ████ ██         ██
#                            ░██░ ░░   █████      ░██░ ░░         ░██
#  █████   ██████  ███████  ██████ ██ ██░░░██    ██████ ██  ██████░██
# ██░░░██ ██░░░░██░░██░░░██░░░██░ ░██░██  ░██   ░░░██░ ░██ ██░░░░ ░██████
#░██  ░░ ░██   ░██ ░██  ░██  ░██  ░██░░██████     ░██  ░██░░█████ ░██░░░██
#░██   ██░██   ░██ ░██  ░██  ░██  ░██ ░░░░░██ ██  ░██  ░██ ░░░░░██░██  ░██
#░░█████ ░░██████  ███  ░██  ░██  ░██  █████ ░██  ░██  ░██ ██████ ░██  ░██
# ░░░░░   ░░░░░░  ░░░   ░░   ░░   ░░  ░░░░░  ░░   ░░   ░░ ░░░░░░  ░░   ░░

#Directorios XDG:
set -x XDG_DATA_HOME "$HOME/.local/share"
set -x XDG_CACHE_HOME "$HOME/.cache"
set -x XDG_CONFIG_HOME "$HOME/.config"

#EDITOR, SHELLS, PATHs comunes
set -x EDITOR "/usr/bin/nvim"
set -x INITVIM "$XDG_CONFIG_HOME/nvim/init.vim"
set -x SHELL_CURRENT ( readlink /proc/$fish_pid/exe )
set -x TERMINAL "/usr/bin/konsole"
#function term_current;
    #set -l ppid ( ps -o 'ppid=' -p $fish_pid );
    #set -l terminal ( ps -o 'cmd=' -p "$ppid" );
    #echo $terminal
#end
set -x BYOBU_CONFIG_DIR "$XDG_CONFIG_HOME/byobu"
set -x TMUXCONF "$BYOBY_CONFIG_DIR/.tmux.conf"
#set -x PATH "$HOME/.local/bin/:$PATH"
set -x FISHRC "$XDG_CONFIG_HOME/fish/config.fish"
set -x BASHRC "$XDG_CONFIG_HOME/bash/bashrc"
set -x BLERC "$XDG_CONFIG_HOME/blesh/init.sh"
set -x ZSHRC "$XDG_CONFIG_HOME/zsh/.zshrc"
set -x TRASHDIR "$XDG_DATA_HOME/Trash/files"
set -x SH_SCRIPTS "$HOME/prog/scripts"
#set -x PATH "$SH_SCRIPTS:$PATH"
#set -x PATH "$HOME/prog/bc:$PATH"
starship init fish | source     #Prompt starship
function fish_right_prompt -d "Write out the right prompt"
    set _ES $status
    test $_ES -ne 0 \
     && printf "\033[38;5;235m\033[1;38;5;208;48;5;235m %s\033[38;5;1;48;5;235m\342\234\227 \033[m" "$_ES" \
     || printf "\033[38;5;235m\033[1;38;5;47;48;5;235m \342\234\223 \033[m"
end

# Modo vim, que hereda accesos de emacs
function fish_hybrid_key_bindings
    for mode in default insert visual
        fish_default_key_bindings -M $mode
    end
    fish_vi_key_bindings --no-erase
end
set -g fish_key_bindings fish_hybrid_key_bindings

# Accesos rápidos de vim (están en /usr/share/fish/functions/fish_vi_key_bindings.fish)

# Modo normal
bind -s -M insert mñ "if commandline -P; commandline -f cancel; else; set fish_bind_mode default; commandline -f backward-char repaint-mode; end"
bind -s l down-or-search
bind -s k up-or-search
bind -s -M default j backward-char
bind -s -M default ñ forward-char
bind -s u history-search-backward      # Deshacer
bind -s U history-search-forward       # Rehacer
bind -s , repeat-jump
bind -s ';' repeat-jump-reverse
bind -s w backward-word
bind -s W backward-bigword
bind -s b forward-word forward-single-char
bind -s B forward-bigword forward-single-char
bind -s e forward-single-char forward-word backward-char
bind -s E forward-bigword backward-char

# Modo visual
bind -s -M visual -m default mñ end-selection repaint-mode
bind -s -M visual j backward-char
bind -s -M visual ñ forward-char
bind -s -M visual k up-line
bind -s -M visual l down-line
bind -s -M visual w backward-word
bind -s -M visual W backward-bigword
bind -s -M visual b forward-word
bind -s -M visual B forward-bigword
bind -s -M visual e forward-word
bind -s -M visual E forward-bigword

# Modo reemplazar
bind -s -M replace -m default mñ cancel repaint-mode
bind -s -M replace_one -m default mñ cancel repaint-mode

# Cursores modo-vim
set fish_cursor_default block
set fish_cursor_insert line
set fish_cursor_replace_one underscore


set fish_greeting   #Que no aparezca el mensaje de bienvenida

abbr -ag fished "$EDITOR $FISHRC"
abbr -ag fishso "source $FISHRC"
abbr -ag bashed "$EDITOR $BASHRC"
abbr -ag bleshed "$EDITOR $BLERC"
abbr -ag zshed "$EDITOR $ZSHRC"
abbr -ag nvimed "$EDITOR $INITVIM"

#Colores
alias ls 'lsd --group-dirs first --date +"%d/%m/%y %H:%M:%S"'
abbr -ag la 'ls -Alh'
abbr -ag ll 'ls -lh'
abbr -ag dir 'ls'
abbr -ag vdir 'ls -lh'
abbr -ag cat 'bat --pager=never'
abbr -ag sudo 'sudo '
abbr -ag grep 'grep --color=auto'
abbr -ag egrep 'grep -E --color=auto'
abbr -ag fgrep 'grep -F --color=auto'
abbr -ag rgrep "grep -RHIni --color=auto"
abbr -ag regrep "grep -ERHIni --color=auto"
abbr -ag rfgrep "grep -FRHIni --color=auto"
abbr -ag diff 'diff --color=auto'
abbr -ag ip 'ip -color=auto'
abbr -ag tree 'tree -Ca'
alias treels 'lsd --group-dirs first --date +"%d/%m/%y %H:%M:%S" --tree -Ah'
abbr -ag treela 'treels -l'
abbr -ag treedu 'dust'
abbr -ag treels-L 'treels --depth'
abbr -ag treela-L 'treels -l --depth'
abbr -ag treedu-L 'dust --depth'
abbr -ag pup 'pup --color'
abbr -ag cdu 'cdu -i -dh'
#abbr -ag cdu 'cdu -i -dh * -ls'
#abbr -ag du 'du -idh'
#abbr -ag tmux "tmux -2"  #Para que tmux reconozca los colores correctos?
set -x SYSTEMD_COLORS 'true'
set -x JQ_COLORS "1;30:4;38;5;202:4;38;5;202:1;38;5;129:3;1;32:1;38;5;221:1;31"
set -x GCC_COLORS 'error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
abbr -ag ptpython 'ptpython --dark-bg'

function cdd; pushd $argv > /dev/null; end
abbr -ag dirs "dirs | tr ' ' '\n'"
alias mkfile "install -D /dev/null"
abbr -ag rmm 'shred -zu'
abbr -ag gs "git status"
abbr -ag c "clear"
abbr -ag e "exit"
abbr -ag bc "bc -l"
alias shell "echo $SHELL_CURRENT"
abbr -ag , "prevd"
abbr -ag . "nextd"
abbr -ag .. "cd .."
abbr -ag pc "proxychains"
abbr -ag sc "systemctl"
abbr -ag za "zathura"
abbr -ag lo "libreoffics"
abbr -ag xd "xdotool"
alias open 'xdg-open . 2> /dev/null &'

set -x FFMPEG_DATADIR "$XDG_CONFIG_HOME/ffmpeg"
set -x RIPGREP_CONFIG_PATH "$XDG_CONFIG_HOME/ripgrep/ripgreprc"
set -x PERL5LIB "/usr/local/share/perl5:/usr/local/share/perl5/x86_64-linux-thread-multi"

#fzf
function fhis; history|fzf|read slct; if [ $slct ]; commandline $slct; else; commandline ''; end; end

#Eliminar ficheros:
set -x LESSHISTFILE -

#Limpieza de directorios no-XDG:
set -x CONDARC "$XDG_CONFIG_HOME/conda/condarc"
set -x ATOM_HOME "$XDG_DATA_HOME/atom"
set -x BASH_COMPLETION_USER_FILE "$XDG_CONFIG_HOME/bash-completion/bash_completion"
set -x CARGO_HOME "$XDG_DATA_HOME/cargo"
set -x DOCKER_CONFIG "$XDG_CONFIG_HOME/docker"
set -x MACHINE_STORAGE_PATH "$XDG_DATA_HOME/docker-machine"
set -x ELECTRUMDIR "$XDG_DATA_HOME/electrum"
set -x EM_CONFIG "$XDG_CONFIG_HOME/emscripten/config"
set -x EM_CACHE "$XDG_CACHE_HOME/emscripten/cache"
set -x EM_PORTS "$XDG_DATA_HOME/emscripten/cache"
set -x GNUPGHOME "$XDG_DATA_HOME/gnupg"
set -x GOPATH "$XDG_DATA_HOME/go"
set -x GRADLE_USER_HOME "$XDG_DATA_HOME/gradle"
set -x GTK_RC_FILES "$XDG_CONFIG_HOME/gtk-1.0/gtkrc"
set -x GTK2_RC_FILES "$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
set -x IMAPFILTER_HOME "$XDG_CONFIG_HOME/imapfilter"
set -x IPYTHONDIR "$XDG_CONFIG_HOME/jupyter"
set -x JUPYTER_CONFIG_DIR "$XDG_CONFIG_HOME/jupyter"
set -x IRBRC "$XDG_CONFIG_HOME/irb/irbrc"
#set -x _JAVA_OPTIONS -Djava.util.prefs.userRoot="$XDG_CONFIG_HOME/java"
set -x KDEHOME "$XDG_CONFIG_HOME/kde4"    # Ya no existe?
set -x LESSKEY "$XDG_CONFIG_HOME/less/lesskey"
set -x LEIN_HOME "$XDG_DATA_HOME/lein"
set -x ICEAUTHORITY "$XDG_CACHE_HOME/ICEauthority"
set -x XCOMPOSEFILE "$XDG_CONFIG_HOME/X11/xcompose"
set -x XCOMPOSECACHE "$XDG_CACHE_HOME/X11/xcompose"
set -x MATHEMATICA_USERBASE "$XDG_CONFIG_HOME/mathematica"
set -x MAXIMA_USERDIR "$XDG_CONFIG_HOME/maxima"
set -x MEDNAFEN_HOME "$XDG_CONFIG_HOME/mednafen"
set -x MOST_INITFILE "$XDG_CONFIG_HOME/mostrc"
set -x MPLAYER_HOME "$XDG_CONFIG_HOME/mplayer"
set -x MYSQL_HISTFILE "$XDG_DATA_HOME/mysql_history"
set -x MYCLI_HISTFILE "$XDG_DATA_HOME/mycli-history"
set -x TERMINFO "$XDG_DATA_HOME/terminfo"
set -x TERMINFO_DIRS "$XDG_DATA_HOME/terminfo"
set -x NODE_REPL_HISTORY "$XDG_DATA_HOME/node_repl_history"
set -x NOTMUCH_CONFIG "$XDG_CONFIG_HOME/notmuch/notmuchrc"
set -x NMBGIT "$XDG_DATA_HOME/notmuch/nmbug"
set -x NPM_CONFIG_USERCONFIG "$XDG_CONFIG_HOME/npm/npmrc"
set -x NUGET_PACKAGES "$XDG_CACHE_HOME/NuGetPackages"
set -x NVM_DIR "$XDG_DATA_HOME/nvm"
set -x OCTAVE_HISTFILE "$XDG_CACHE_HOME/octave-hsts"
set -x OCTAVE_SITE_INITFILE "$XDG_CONFIG_HOME/octave/octaverc"
set -x PARALLEL_HOME "$XDG_CONFIG_HOME/parallel"
set -x PASSWORD_STORE_DIR "$XDG_DATA_HOME/pass"
set -x PSQLRC "$XDG_CONFIG_HOME/pg/psqlrc"
set -x PSQL_HISTORY "$XDG_CACHE_HOME/pg/psql_history"
set -x PGPASSFILE "$XDG_CONFIG_HOME/pg/pgpass"
set -x PGSERVICEFILE "$XDG_CONFIG_HOME/pg/pg_service.conf"
set -x AZURE_CONFIG_DIR "$XDG_DATA_HOME/azure"
set -x GRIPHOME "$XDG_CONFIG_HOME/grip"
set -x PYTHON_EGG_CACHE "$XDG_CACHE_HOME/python-eggs"
set -x PYLINTHOME "$XDG_CACHE_HOME/pylint"
set -x PLTUSERHOME "$XDG_DATA_HOME/racket"
set -x REDISCLI_HISTFILE "$XDG_DATA_HOME/redis/rediscli_history"
set -x REDISCLI_RCFILE "$XDG_CONFIG_HOME/redis/redisclirc"
set -x RLWRAP_HOME "$XDG_DATA_HOME/rlwrap"
set -x GEM_HOME "$XDG_DATA_HOME/gem"
set -x GEM_SPEC_CACHE "$XDG_CACHE_HOME/gem"
set -x RUSTUP_HOME "$XDG_DATA_HOME/rustup"
set -x DOT_SAGE "$XDG_CONFIG_HOME/sage"
set -x SCREENRC "$XDG_CONFIG_HOME/screen/screenrc"
set -x SPACEMACSDIR "$XDG_CONFIG_HOME/spacemacs"
set -x STACK_ROOT "$XDG_DATA_HOME/stack"
set -x TASKDATA "$XDG_DATA_HOME/task"
set -x TASKRC "$XDG_CONFIG_HOME/task/taskrc"
set -x TEXMFHOME "$XDG_DATA_HOME/texmf"
set -x TEXMFVAR "$XDG_CACHE_HOME/texlive/texmf-var"
set -x TEXMFCONFIG "$XDG_CONFIG_HOME/texlive/texmf-config"
set -x UNCRUSTIFY_CONFIG "$XDG_CONFIG_HOME/uncrustify/uncrustify.cfg"
set -x UNISON "$XDG_DATA_HOME/unison"
set -x RXVT_SOCKET "$XDG_RUNTIME_DIR/urxvtd"
set -x VAGRANT_HOME "$XDG_DATA_HOME/vagrant"
set -x VAGRANT_ALIAS_FILE "$XDG_DATA_HOME/vagrant/aliases"
set -x WORKON_HOME "$XDG_DATA_HOME/virtualenvs"
set -x VSCODE_PORTABLE "$XDG_DATA_HOME/vscode"
set -x WEECHAT_HOME "$XDG_CONFIG_HOME/weechat"
set -x WGETRC "$XDG_CONFIG_HOME/wgetrc"
alias wget 'wget --hsts-file="$XDG_CACHE_HOME/wget-hsts"'
set -x WINEPREFIX "$XDG_DATA_HOME/wineprefixes/default"
#set -x XAUTHORITY "$XDG_RUNTIME_DIR/Xauthority"
set -x XAUTHORITY "$HOME/.Xauthority" # La variable de arriba ↑ me causaba problemas
set -x XINITRC "$XDG_CONFIG_HOME/X11/xinitrc"
set -x XSERVERRC "$XDG_CONFIG_HOME/X11/xserverrc"
set -x _Z_DATA "$XDG_DATA_HOME/z"
alias yarn "yarn --use-yarnrc $XDG_CONFIG_HOME/yarn/config"
#Para directorios no-XDG hardcodeados:
alias mongo "mongo --norc --shell $XDG_CONFIG_HOME/mongo/mongorc.js"
set -x JULIA_DEPOT_PATH "$XDG_DATA_HOME/julia"
set -x PYTHONHISTORY "$XDG_CACHE_HOME/python/history" #Todavía no mergeado en Python para ~/.python_history
set -x MOZ_PROFILE_HOME "$XDG_CACHE_HOME/mozilla/firefox" #Todavía no mergeado en Firefox para ~/.mozilla
set -x TS3_CONFIG_DIR "$XDG_CONFIG_HOME/ts3client"
set -x SQLITE_HISTORY "$XDG_DATA_HOME/sqlite_history" #sqlite3 -init "${XDG_CONFIG_HOME}"/sqlite3/sqliterc
set -x VIMPERATOR_INIT ":source $XDG_CONFIG_HOME/vimperator/vimperatorrc"
set -x VIMPERATOR_RUNTIME "$XDG_CONFIG_HOME/vimperator"
set -x ZDOTDIR "$XDG_CONFIG_HOME/zsh"
#set -x HISTFILE "$XDG_DATA_HOME"/zsh/history

#Para reportar/buscar mensajes de error en inglés:
set -x LC_MESSAGES C           # Puedes poner solo esta, y si no funciona ↓
#set -x LANG en_US.UTF-8        # O estas dos, que cambian todo el lenguaje de tu equipo
#set -x LANGUAGE en_US:en
set -x TZ 'UTC-0'   # Uso horario



