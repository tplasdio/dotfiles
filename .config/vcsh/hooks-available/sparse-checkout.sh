#!/bin/sh

: "${XDG_CONFIG_HOME:="$HOME/.config"}"

git config core.sparseCheckout true
rm -f "$GIT_DIR/info/sparse-checkout"
ln -s "$XDG_CONFIG_HOME/vcsh/sparse-checkout" "$GIT_DIR/info/sparse-checkout"
