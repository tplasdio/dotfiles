set term wxt background rgb "#2e3547"  # Background color
set border linecolor rgb "white"
set tics font "Liberation Serif,13"
#set tics textcolor rgb "white"
#set key textcolor variable
set key textcolor "white"

set grid linecolor rgb "#e4e4e4"
load 'linecolors/myawesome.plt'

# vim: ft=gnuplot
