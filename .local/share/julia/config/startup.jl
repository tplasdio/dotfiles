#!/usr/bin/julia
"""
Execute a quoted string shell command with
a shell, "sh" by default
"""
function sh(cmd::String, shell::String = raw"sh", args...; kw...)::Nothing
	run(`$shell -c $cmd`, args..., kw...)
	nothing
end

"""
List directories, with lsd || ls || dir
"""
ls(args...)::Nothing = begin
	try
		run(`lsd $args`)
	catch; try
		run(`ls $args`)
	catch
		run(`dir $args`)
	end end
	nothing
end

"""
List directories, in long listing format
"""
ll(args...)::Nothing = ls("-l", args...)

"""
List directories, in long listing human readable format,
including hidden files (starting with .)
"""
la(args...)::Nothing = ls("-l", "-A", "-h", args...)

"""
Clear the screen
"""
clear()::Nothing = begin
	try
		run(`clear`)
	catch
		run(`cls`)
	end
	nothing
end


bat(args...)::Nothing = begin
	run(`bat $args`)
	nothing
end

macro ls(a...) ls(a...) end
macro ll(a...) ll(a...) end
macro la(a...) la(a...) end
macro clear() clear() end
macro exit() exit() end
macro exit(es::Int64) exit(es) end
macro pwd() pwd() end
macro cd() cd() end
macro cd(directory::String) cd(directory) end

# import OhMyREPL
