<p align="center">
  <h1 align="center">Dotfiles</h1>
  <p align="center">
    My configuration files for various programs
  </p>
</p>

## Table of Contents

- [📰 Overview](#overview)
- [⭐ App configurations](#app-configurations)
- [💻 Programming languages and shell configurations](#programming-languages-and-shell-configurations)
- [🖥 System configurations](#system-configurations)
  - [✨ Default applications](#default-applications)
  - [🖼 X server](#x-server)
- [👀 See also](#see-also)
- [📝 Licence](#licence)

## 📰 Overview

Here I maintain configuration files of programs that I use or have
used in my Arch Linux system.

Some programs' configurations that I think deserve
their own repositories are not here, like:

App | Repository | Description
:---:|:---:|:---
AwesomeWM | [awesomewm-config](https://codeberg.org/tplasdio/awesomewm-config) | Awesome window manager
Bash | [bash-config](https://codeberg.org/tplasdio/bash-config) | Bash command-line shell
Neovim | [neovim-config](https://codeberg.org/tplasdio/neovim-config) | Neovim text editor
Lf | [lf-config](https://codeberg.org/tplasdio/lf-config) | Lf terminal file manager
Mpv | [mpv-config](https://codeberg.org/tplasdio/mpv-config) | Mpv video/audio player
Dash | [dash-config](https://codeberg.org/tplasdio/dash-config) | Dash shell and POSIX functions
*many* | [root-dotfiles](https://codeberg.org/tplasdio/root-dotfiles) | Config files from /

*Note: If you'd like to know how I easily maintain all of them separately,
I use __[vcsh](https://github.com/RichiH/vcsh)__,
and __[yadm](https://github.com/TheLocehiliosan/yadm)__ for my root dotfiles*

Also, my scripts are not included here, but in [this repo](https://codeberg.org/tplasdio/scripts)

## ⭐ App configurations

App | Files | Short description
:---:|:---:|:---
sxhkd | `sxhkdrc` | Desktop keymaps (keyboard shortcuts)
alacritty | `alacritty.yml` | Terminal keymaps, appearance
rofi | `config.rasi`<br>`test_arc_dark.rasi` | Launcher modes, keymaps<br>Launcher appearance
vimiv | `keys.conf`<br>`vimivrc`<br>`vimiv.conf` | Image viewer keymaps and options
htop | `htoprc` | Process manager appearance
zathura | `zathurarc` | PDF reader keymaps, appearance
less | `lesskey` | Pager keymaps, appearance
byobu (tmux) | `.tmux.conf` | Multiplexer keymaps, appearance
picom | `picom.conf` | Compositor opacity, shadows, effects
git | `config`<br>`gitalias.txt` | Git config<br>Many git aliases and short scripts
vcsh | `vcsh/hooks*`<br>`sparse-checkout` | Git hooks for vcsh repos<br>Sparse/Hidden files allowed in vcsh repos root dirs
yadm | `bootstrap` | Sparse/Hidden files allowed in yadm repo root dir
calcurse | `conf`<br>`keys` | Calendar keymaps, appearance
konsole | `konsolerc`<br>`*.colorscheme` | Terminal keymaps, appearance<br>Terminal color schemes
kitty | `kitty.conf` | Terminal keymaps, appearance
terminator | `terminator/config` | Terminal keymaps, appearance
newsboat | `newsboat/config` | RSS reader keymaps, appearance
urxvt | `Xresources/urxvt` | Terminal keymaps, appearance
gitk | `git/gitk` | Git repo browser appearance
git-cliff | `cliff.toml` | Git changelog template appearance
ytfzf | `conf.sh` | Youtube scraping options
starship | `starship.toml` | Prompt appearance, widgets
nano | `nanorc` | Text editor keymaps, appearance
flameshot | `flameshot.ini` | Screenshoter contrast, saving
inxi | `inxi.conf` | System information appearance
readline | `inputrc` | Completion options
screenkey | `screenkey.json` | Key shower appearance
xournalpp | `settings.xml` | Notetaker tool defaults, appearance
redshift | `redshift.conf` | Color temperature
libreoffice | `*.xcu` | Office programs color scheme
lsd | `config.yaml`<br>`icons.yaml` | Lsd default options<br>Icons for filetypes and extensions
ripgrep | `ripgreprc` | Ripgrep default options
bat | `bat/config` | File syntax highlighter
neofetch | `config.conf` | System information appearance

## 💻 Programming languages and shell configurations

Language/App | Files | Short description
:---:|:---:|:---
Python | `pythonrc.py` | Runcom. Run on every REPL, lazy imports
R | `Rprofile`<br>`Renviron` | Runcom, lazy imports<br>Environment variables
Julia | `startup.jl` | Runcom
Fish | `config.fish`<br>`fish_variables` | Runcom<br>Environment variables
Ion | `initrc` | Runcom
MySQL | `myclirc` | REPL client appearance
Jupyter | `jupyter_server_config.json`<br>`themes.jupyterlab-settings` | <br> JupyterLab appearance
Gnuplot | `gnuplotrc` | Graphics appearance
Matplotlib | `matplotlibrc` | Graphics appearance

\* Some languages may have system configs, check my root dotfiles repo.

## 🖥 System configurations

### ✨ Default applications

I also maintain desktop entries for my default applications in `.local/share/applications/*.desktop`.
These are to be used by a [resource opener](https://wiki.archlinux.org/title/Resource_opener)
to associate with a file mime type, extension, etc. to open a specific file.

These are the descriptive:
 - `FILE_MANAGER.desktop`
 - `BROWSER.desktop`
 - `EDITOR.desktop`
 - `IMAGE_EDITOR.desktop`
 - `AUDIO_PLAYER.desktop`
 - `VIDEO_PLAYER.desktop`
 - `READER.desktop`
 - `EMAIL_CLIENT.desktop`
 - `TORRENT_CLIENT.desktop`

The reason I do this is to manage them more easily. Now if, for example, I want to
change my default email client, I just change the command in the `EMAIL_CLIENT.desktop`
and don't have to change anything in my resource opener, whatever this may be.

On that note, I maintain these opening associations:

Resource opener | Files |
:---:|:---:
`xdg-open`| `mimeapps.list`

Also these names may be used as environment variables so that a
file manager could be fined-tuned to use a different default app
in a specific case.

You may also be interested in my
[opener](https://codeberg.org/tplasdio/lf-config/src/branch/main/.config/lf/opener)
script and my
[xdg-mime-env](https://codeberg.org/tplasdio/scripts/src/branch/main/scripts/xdg-mime-env)
script

### 🖼 X server

I maintain some files that modify the X server initialization or configuration,
mostly located in `.config/X11/*`. These are:

File | Short description
:---:|---
`xmodmap` | Keyboard layout
`xcompose` | Keybord configurations
`Xresources` | Configuration parameters for X apps
`xsettingsd.conf` | Daemon configurations for Xorg apps, when not running one
`shelldm` | Shell "display manager" script
`xserverrc` | Shell script that starts X
`xinitrc` | Shell script running when X starts
`user-dirs.dirs` | XDG user directories (in your ~)

I don't use a display manager to start my X server, instead I use
the [xinit](https://wiki.archlinux.org/title/Xinit) program. But
instead of running `startx` from my tty on login, I made
a tiny shell script `shelldm` that shows a menu selection for
the available desktops/window managers, and then startx it.

To set this up, you have to run this script from your system
shell's system runcom. So for example for me, my system shell
is `dash` and I put a line like this:

```sh
case "$(tty)" in
  "/dev/tty1")
    exec "${HOME}/.config/X11/shelldm"
esac
```

at the end of the `/etc/profile` file.

You can check my root dotfiles for details.

## 👀 See also
- [Dotfiles](https://wiki.archlinux.org/title/Dotfiles)
  - [Arch users dotfiles](https://wiki.archlinux.org/title/Dotfiles#User_repositories)
- [Resource openers](https://wiki.archlinux.org/title/Resource_opener)
- [Console display managers](https://wiki.archlinux.org/title/Login_manager#Console)
- [IANA MIME types](https://www.iana.org/assignments/media-types/media-types.xhtml)

## 📝 Licence
GPLv3-or-later (to the extend that *mostly* config files are copyrightable)
